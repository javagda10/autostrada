package com.sda.autostrada;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDateTime;

@AllArgsConstructor
@Data
public class DanePojazdu {
    private LocalDateTime czasWjazdu;
    private LocalDateTime czasWyjazdu;
    private String numerRejestracyjny;

    public DanePojazdu(LocalDateTime czasWjazdu, String numerRejestracyjny) {
        this.czasWjazdu = czasWjazdu;
        this.numerRejestracyjny = numerRejestracyjny;
    }
}

package com.sda.autostrada;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Autostrada {
    private Map<String, DanePojazdu> rejestr;
//    private List<DanePojazdu> rejestr = new ArrayList<>();

    public Autostrada() {
        rejestr = new HashMap<>();
    }

    public void wjazdPojazdu(String numeryRejestracyjne) {
        DanePojazdu danePojazdu = new DanePojazdu(LocalDateTime.now(), numeryRejestracyjne);
        rejestr.put(numeryRejestracyjne, danePojazdu);
//        rejestr.add(danePojazdu);
    }

    public void wyjazdPojazdu(String numerRejestracji) {
        if (rejestr.containsKey(numerRejestracji)) {
            DanePojazdu dane = rejestr.get(numerRejestracji);
            dane.setCzasWyjazdu(LocalDateTime.now());

            Duration czasPrzejazdu = Duration.between(dane.getCzasWjazdu(), dane.getCzasWyjazdu());

            double minutes = czasPrzejazdu.getSeconds() / 60.0;
            System.out.println("Czas przejazdu samochodu: " + minutes);

            // usuwamy z rejestru
            rejestr.remove(numerRejestracji);
        }else{
            System.out.println("Nie zarejestrowany pojazd!");
        }

        /*for (DanePojazdu pojazd :rejestr) {
            if(pojazd.getNumerRejestracyjny().equals(numerRejestracji)){
                DanePojazdu dane = pojazd;
                dane.setCzasWyjazdu(LocalDateTime.now());
                Duration czasPrzejazdu = Duration.between(dane.getCzasWjazdu(), dane.getCzasWyjazdu());

                double minutes = czasPrzejazdu.getSeconds() / 60.0;
                System.out.println("Czas przejazdu samochodu: " + minutes);
                rejestr.remove(pojazd);
                break;
            }
        }*/

    }

}
